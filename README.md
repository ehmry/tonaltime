# Tonaltime

A Syndicated clock actor.

Publishes a form of [Tonal time](https://en.wikipedia.org/wiki/Hexadecimal_time) that counts down to the next solar midnight local to some given coordinates.

- `<tonaltime @raw int @hex string>`

Four solar assertions are published in traditational local time.
- `<midnight <rfc3339 @hh:mm:ss string>>`
- ` <sunrise <rfc3339 @hh:mm:ss string>>`
- `    <noon <rfc3339 @hh:mm:ss string>>`
- `  <sunset <rfc3339 @hh:mm:ss string>>`

The actor implements the gatekeeper protocol.
Use a step in the form `<tonaltime { lat: 00.000 lon: 00.000 }>` to resove
to a dataspace where the clock and solar assertions are made.

To run in standalone mode pass a [Preserves](http://preserves.dev)
dictionary with `lat` and `lon` values specifying coordinates to
align with.

```sh
$ tonaltime '{ lon:-3.571702 lat:36.998503}'
```

The optional `{ bits: ℕ }` argument sets the number of bits used by the clock and affects how often clock ticks are issued.
This number is currently clamped to a value between 1 and 16.

## Example

### Configuration
```
# syndicate-server configuration

# Listen on a Unix socket.
<require-service <relay-listener <unix "/run/user/1000/syndicate"> $gatekeeper>>

# Create a dataspace for resolving services.
let ?servicespace = dataspace

# Bind that dataspace with a sturdyref.
<bind <ref { oid: services key: #x"" }> $servicespace #f>

# Observe resolve requests.
$servicespace ? <resolve <tonaltime ?detail> ?obs> [
  # Start the daemon.
  $config <require-service <daemon tonaltime>>

  # Forward the request onto the daemon.
  $config ? <service-object <daemon tonaltime> ?cap> [
    $cap <resolve <tonaltime $detail> $obs>
  ]
]

# Specify how the daemon is started.
<daemon tonaltime {
  argv: "tonaltime"
  protocol: application/syndicate
  clearEnv: #t
}>

```

### Access

To access the service configured above use the route `<route [<unix "/run/user/1000/syndicate">] <ref {oid: services sig: #x"279857dc7ab625a174a797934cea4f2d"}> <tonaltime { lon: 15.0 lat: 55.1 }>>`
