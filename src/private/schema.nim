
import
  preserves, std/options

type
  TonaltimeStep* {.preservesRecord: "tonaltime".} = object
    `detail`*: TonaltimeDetail
  Sunrise* {.preservesRecord: "sunrise".} = object
    `time`*: Time
  TonaltimeDetailBits* = Option[BiggestInt]
  TonaltimeDetailLat* = float
  TonaltimeDetailLon* = float
  `TonaltimeDetail`* {.preservesDictionary.} = object
    `bits`*: Option[BiggestInt]
    `lat`*: float
    `lon`*: float
  Sunset* {.preservesRecord: "sunset".} = object
    `time`*: Time
  Noon* {.preservesRecord: "noon".} = object
    `time`*: Time
  PrecisionBitsKind* {.pure.} = enum
    `present`, `absent`
  PrecisionBitsPresent* {.preservesDictionary.} = object
    `bits`*: BiggestInt
  PrecisionBitsAbsent* {.preservesDictionary.} = object
  `PrecisionBits`* {.preservesOr.} = object
    case orKind*: PrecisionBitsKind
    of PrecisionBitsKind.`present`:
      `present`*: PrecisionBitsPresent
    of PrecisionBitsKind.`absent`:
      `absent`*: PrecisionBitsAbsent
  TonalTime* {.preservesRecord: "tonaltime".} = object
    `num`*: BiggestInt
    `hex`*: string
  Midnight* {.preservesRecord: "midnight".} = object
    `time`*: Time
  Time* {.preservesRecord: "rfc3339".} = object
    `partialTime`*: string
proc `$`*(x: TonaltimeStep | Sunrise | TonaltimeDetail | Sunset | Noon |
    PrecisionBits |
    TonalTime |
    Midnight |
    Time): string =
  `$`(toPreserves(x))

proc encode*(x: TonaltimeStep | Sunrise | TonaltimeDetail | Sunset | Noon |
    PrecisionBits |
    TonalTime |
    Midnight |
    Time): seq[byte] =
  encode(toPreserves(x))
