# SPDX-FileCopyrightText: ☭ 2022 Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/[math, times]

func forceRange[T: SomeNumber](v, max: T): T =
  if v < 0: v + max
  elif v >= max: v - max
  else: v

proc solarTime(lon, lat: float; dt: DateTime; zenith: float; isRiseTime: bool): DateTime =
  # Adapted from https://stackoverflow.com/questions/19615350/calculate-sunrise-and-sunset-times-for-a-given-gps-coordinate-within-postgresql
  # TODO: don't just copy from stack-overflow
  # TODO: seconds precision, not minutes
  let dt = utc(dt)

  const TO_RAD = Pi / 180.0

  let lngHour = lon / 15
      # convert the longitude to hour value and calculate an approximate time

  let t =
      if isRiseTime: dt.yearday.float + ((6 - lngHour) / 24)
      else: dt.yearday.float + ((18 - lngHour) / 24)

  let M = (0.9856 * t) - 3.289
      # calculate the Sun's mean anomaly

  let L = forceRange(M + (1.916 * sin(TO_RAD * M)) + (0.02 * sin(TO_RAD * 2 * M)) + 282.634, 360.0)
      # calculate the Sun's true longitude

  var RA = forceRange((1 / TO_RAD) * arctan(0.91764 * tan(TO_RAD * L)), 360)
      # calculate the Sun's right ascension

  let Lquadrant = floor(L / 90) * 90
  let RAquadrant = floor(RA / 90) * 90
  RA = RA + (Lquadrant - RAquadrant)
    # calculate the Sun's right ascension

  RA = RA / 15
    # right ascension value needs to be converted into hours

  let
    sinDec = 0.39782 * sin(TO_RAD * L)
    cosDec = cos(arcsin(sinDec))
      # calculate the Sun's declination

  let cosH = (cos(TO_RAD * zenith) - (sinDec * sin(TO_RAD * lat))) / (cosDec * cos(TO_RAD * lat))

  if cosH < -1 or 1 < cosH:
    raise newException(Defect, "no sunrise or sunset for this day - cosH=" & $cosH)

  var H =
    if isRiseTime:
      360 - (1 / TO_RAD) * arccos(cosH)
    else:
      (1 / TO_RAD) * arccos(cosH)
  H = H / 15
    # finish calculating H and convert into hours

  let T = H + RA - (0.06571 * t) - 6.622
    # calculate local mean time of rising/setting

  let UT = forceRange(T - lngHour, 24)
    # adjust back to UTC

  dateTime(
    year = dt.year,
    month = dt.month,
    monthday = dt.monthday,
    hour = int forceRange(UT, 24),
    minute = int (UT * 60) mod 60,
    zone = utc())

type HorizonTimes* = tuple[sunrise: DateTime, sundown: DateTime]

proc horizonTimes*(lon, lat: float; dt = now(); zenith = 90.833): HorizonTimes =
  ## Calculate when the sun crosses the horizon for a given coordinates and
  ## `DateTime`. A zenith of 90.833° is an approximation to correct for
  ## atmospheric refraction and the size of the solar disc.
  result.sunrise = local solarTime(lon, lat, dt, zenith, isRiseTime = true)
  result.sundown = local solarTime(lon, lat, dt, zenith, isRiseTime = false)
