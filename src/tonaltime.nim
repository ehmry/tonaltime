# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[cmdline, math, oserrors, options, posix, times],
  pkg/sys/[handles, ioqueue],
  preserves, preserves/sugar,
  syndicate/protocols/[gatekeeper, sturdy],
  syndicate , syndicate/relays,
  ./private/[schema, solartimes]

from std/strutils import toHex

proc echo(args: varargs[string, `$`]) {.used.} =
  stderr.writeLine(args)

type Time = posix.Time

{.pragma: timerfd, importc, header: "<sys/timerfd.h>".}

proc timerfd_create(clock_id: ClockId, flags: cint): cint {.timerfd, used.}
proc timerfd_settime(ufd: cint, flags: cint,
                      utmr: var Itimerspec, otmr: var Itimerspec): cint {.timerfd, used.}
proc timerfd_gettime(ufd: cint, curr: var Itimerspec): cint {.timerfd, used.}

var
  TFD_NONBLOCK {.timerfd.}: cint
  TFD_CLOEXEC {.timerfd.}: cint
  TFD_TIMER_ABSTIME {.timerfd.}: cint

type
  Clock = ref object
    dataspace: Cap
    midnight: DateTime
    timeHandle: syndicate.Handle
    timer: cint

const
  secPerDay = 24 * 60 * 60
  milisecPerDay = secPerDay * 1_000
  nanosecPerSec = 1_000_000_000
  nanosecPerDay = secPerDay * nanosecPerSec
  milisecPerHexsec = milisecPerDay div 0x1_00_00
  nanosecPerHexsec = nanosecPerDay div 0x1_00_00

func nanosecPerMask(bits: Natural): int64 =
  nanosecPerDay div (1 shl bits)

proc divmodNano(nanosec: int64): (posix.Time, int64) =
  ## Seperate `nanosec` into seconds and nanoseconds.
  let (q, r) = divmod(nanosec, nanosecPerSec)
  (posix.Time q, r)

proc setTimer(clock: Clock; bits: range[1 .. 16]) =
  assert clock.timer == 0
  let nanoInterval = nanosecPerMask(bits)
  var its, old: Itimerspec
  clock.timer = timerfd_create(CLOCK_REALTIME, TFD_NONBLOCK or TFD_CLOEXEC)
  if clock.timer < 0:
    raiseOSError(osLastError(), "failed to acquire timer descriptor")
  if clock_gettime(CLOCK_REALTIME, its.it_value) < 0:
    raiseOSError(osLastError(), "clock_gettime")
  let
    midnightNano = clock.midnight.toTime.toUnix * nanosecPerSec
    nowNano = its.it_value.tv_sec.int64 * nanosecPerSec + its.it_value.tv_nsec.int64
    sinceMidnightNano = nowNano - midnightNano
    nextEdgeNano = nowNano + nanoInterval - (sinceMidnightNano mod nanoInterval)
  (its.it_value.tv_sec, its.it_value.tv_nsec) = divmodNano nextEdgeNano
  (its.it_interval.tv_sec, its.it_interval.tv_nsec) = divmodNano nanoInterval
  if timerfd_settime(clock.timer, TFD_TIMER_ABSTIME, its, old) < 0:
    raiseOSError(osLastError(), "failed to set timeout")

proc update(clock: Clock) =
  run(clock.dataspace) do (turn: Turn):
    let
      milisecs = (now() - clock.midnight).inMilliseconds
      hexsecs = 0xFF_FF - ((milisecs div milisecPerHexsec) and 0xFF_FF)
      hexStr = toHex(hexsecs shr 12, 1) & "_" & toHex(hexsecs shr 4, 2) & "_" & toHex(hexsecs, 1)
    replace(turn, clock.dataspace, clock.timeHandle, TonalTime(num: hexsecs, hex: hexStr))

proc loop(clock: Clock) {.asyncio.} =
  let fd = clock.timer
  while clock.timer > -1:
    update(clock)
    wait(FD fd, Read)
    var n: uint64
    discard read(fd, addr n, sizeof n)
  discard close(fd)

proc stop(clock: Clock) =
  clock.timer = -1 # break the timer loop

proc bootClock(turn: Turn; args: TonaltimeDetail): Clock =
  let
    horizon = horizonTimes(args.lon, args.lat, now())
    dayDur = horizon.sundown - horizon.sunrise
    noon = horizon.sunrise + (dayDur div 2)
    midnight = noon - initDuration(hours = 12)
    bits = clamp(args.bits.get(16).int, 1, 16)

  let clock = Clock(
      dataspace: newDataspace(turn),
      midnight: midnight,
    )
  clock.setTimer(bits)
  turn.facet.onStop do (t: Turn):
    clock.timer = -1 # break the timer loop

  discard trampoline:
    whelp clock.loop()

  const partialTime = initTimeFormat("HH:mm:ss")
  template publish(label: string; time: DateTime) =
    discard publish(turn, clock.dataspace,
        initRecord(label, initRecord("rfc3339", %time.format(partialTime))))

  publish("sunrise", horizon.sunrise)
  publish("sunset", horizon.sundown)
  publish("noon", noon)
  publish("midnight", midnight)
    # TODO: recalculate when each event passes
  clock

proc observeOnly: Caveat =
  """<reject <not <rec Observe [<_> <_>]>>>""".parsePreserves.preservesTo(Caveat).get

case paramCount()
of 1:
  var
    pr: Value
    detail: TonaltimeDetail
  try: pr = paramStr(1).parsePreserves
  except: quit("failed to parse Preserves argument")
  if not detail.fromPreserves(pr):
    quit("""invalid command-line parameter, expected "{ lat:… lon:… }"""")
  runActor("main") do (turn: Turn):
    let clock = bootClock(turn, detail) # Create a clock for these args.
    onPublish(turn, clock.dataspace, match(TonalTime, { 1: grab() })) do (hex: string):
      stdout.writeLine(hex)
      stdout.flushFile()
of 0:
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; relay: Cap):
      during(turn, relay, Resolve?:{0: TonaltimeStep.grabType }) do (step: TonaltimeStep):
        let actor = spawnActor(turn, "clock") do (turn: Turn):
          let
            clock = bootClock(turn, step.detail) # Create a clock for these args.
            cap = clock.dataspace.attenuate observeOnly()
          during(turn, relay, Resolve?:{0: ?step, 1: grab()}) do (observer: Cap):
            discard publish(turn, observer,
                ResolvedAccepted(responderSession: cap))
                  # Publish the clock to the observers of these args.
else:
  quit("invalid command-line parameters")
